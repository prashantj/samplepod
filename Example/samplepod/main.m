//
//  main.m
//  samplepod
//
//  Created by prashantj on 04/09/2019.
//  Copyright (c) 2019 prashantj. All rights reserved.
//

@import UIKit;
#import "AUAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AUAppDelegate class]));
    }
}
