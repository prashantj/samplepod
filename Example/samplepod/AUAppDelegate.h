//
//  AUAppDelegate.h
//  samplepod
//
//  Created by prashantj on 04/09/2019.
//  Copyright (c) 2019 prashantj. All rights reserved.
//

@import UIKit;

@interface AUAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
