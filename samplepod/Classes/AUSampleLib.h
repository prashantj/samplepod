//
//  AUSampleLib.h
//  samplepod
//
//  Created by MindstixSoftwareLabss-MacBook-Pro on 09/04/19.
//

#import <Foundation/Foundation.h>

@interface AUSampleLib: NSObject
+(NSString *) getTitle;
+(NSInteger *) getSum:(NSInteger *)value1 value2:(NSInteger)val;
@end
