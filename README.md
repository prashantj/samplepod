# samplepod

[![CI Status](https://img.shields.io/travis/prashantj/samplepod.svg?style=flat)](https://travis-ci.org/prashantj/samplepod)
[![Version](https://img.shields.io/cocoapods/v/samplepod.svg?style=flat)](https://cocoapods.org/pods/samplepod)
[![License](https://img.shields.io/cocoapods/l/samplepod.svg?style=flat)](https://cocoapods.org/pods/samplepod)
[![Platform](https://img.shields.io/cocoapods/p/samplepod.svg?style=flat)](https://cocoapods.org/pods/samplepod)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

samplepod is available through [CocoaPods](https://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod 'samplepod'
```

## Author

prashantj, prashant.joshi60@gmail.com

## License

samplepod is available under the MIT license. See the LICENSE file for more info.
