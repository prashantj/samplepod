//
//  AUViewController.m
//  samplepod
//
//  Created by prashantj on 04/09/2019.
//  Copyright (c) 2019 prashantj. All rights reserved.
//

#import "AUViewController.h"
#import "AUSampleLib.h"

@interface AUViewController ()

@end

@implementation AUViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    
    NSString *title = @"";
   
    
    NSLog(@"I am inside View Did Load");
    
    title = [AUSampleLib getTitle];
    
    NSLog(@"I am inside View Did Load");
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
